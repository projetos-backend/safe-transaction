package com.pedro.safetransaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SafeTransactionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SafeTransactionApplication.class, args);
	}

}
